import React, { Children } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
  useHistory,
  Button
} from "react-router-dom";
import Login from  './Pages/Auth/Login'
import SpellList from './Spells/SpellList'

const firebase = require('firebase');



export default class App extends React.Component {
  
  constructor(props){
    super(props)
    //firebase.apps.length ? firebase.initializeApp(firebaseConfig) : firebase.app();

    this.state={user:undefined}
  }

  componentDidMount(){
    firebase.auth().onAuthStateChanged(user => {
      console.log('user', user)
      if (user) {
        this.setState({user})
      } else {
        this.setState({user:undefined})
      }
    });
  }

  Logout(callback) {
  
    //firebase.auth().signOut()
    callback()
  }
  render(){
  var {history} = this.props;
  return (
    <Router>
      <div className="Container">
        <nav>
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/login">Login</Link>
            </li>
            <li>
              user : {this.state.user? firebase.auth().currentUser.email : 'Not connected'}
            </li>
            <li>
              <button onClick={()=>{this.Logout(() => history.push("/"))}}>Logout</button>
            </li>
          </ul>
        </nav>

        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
        <Switch>
          <Route path="/login">
            <Login />
          </Route>
          <Route path="/">
            <SpellList></SpellList>
          </Route>
        </Switch>
      </div>
    </Router>
  );
}
}

function Home(props) {
  return  {...props.children}
}



