import firebase from 'firebase'

 const firebaseConfig = {
    apiKey: "AIzaSyBGWMWhCIkDavOUBTcCR8RABhqCrgVNT4w",
    authDomain: "react-firebase-0.firebaseapp.com",
    databaseURL: "https://react-firebase-0.firebaseio.com",
    projectId: "react-firebase-0",
    storageBucket: "react-firebase-0.appspot.com",
    messagingSenderId: "820175269634",
    appId: "1:820175269634:web:e04f53be7e8cc5b4ac5c1b"
  };

firebase.initializeApp(firebaseConfig)

export default firebase