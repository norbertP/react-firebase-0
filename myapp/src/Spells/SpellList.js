import React from 'react'
import firebase from '../_helpers/firebase'
//import { } from 'react-dom'

export default class SpellList extends React.Component {
    
    constructor(props) {
        super(props)
        this.state = {
            itemList: [],
            loading: true,
            error: undefined
        }
        this.collection = firebase.firestore().collection('spells')
    }

    componentDidMount() {
        //Load all items in the collection
        this.collection.get()
            .then((list) => {
                console.log('ici', list)
                this.setState({
                    loading: false,
                    itemList: list.docs.map(doc => doc.data())
                })
            })
            //.error(error => this.setState({error}))
    }

    render2() {
        return (<div>Hello</div>)
    }
    render() {
        const { itemList } = this.state
        return (
        
        <div>
        { itemList &&
            <ul>
                {itemList.map(item => <li key={item._id} > {item.name}</li>)}
            </ul>
        }
        </div>
        )
    }

}